AS Docker - Projects Environment
===

Global `docker-compose.yml` to run on specified environment once (globally).

* proxy - [Traefik](https://docs.traefik.io/) local **80/443** port and handling HTTP/HTTPS connections to webapp (e.g. nginx) containers
* ssh - [sshd](https://hub.docker.com/r/panubo/sshd/) local **2222** port and add Your pub-key to enable tunneling to all containers by `container_name`
* portainer - [Portainer](https://portainer.io) docker browser GUI, use Traefik to handle `portainer.test` local domain


Files tree
---
```
.
├── local
│   └── docker-compose.yml
├── prod
│   ├── traefik
│   │   ├── acme.json
│   │   └── traefik.toml
│   └── docker-compose.yml
```
depends on Your environment, use proper `docker-compose.yml`


Install (localhost)
---
* try to keep the below structure at your home:
```
.
└── Projects
    ├── example.com
    │   ├── docker
    │   │   └── container
    │   ├── Yakefile
    │   └── docker-compose.yml
    └── docker-compose.yml
```
* run `docker network create global` once
* make sure Your local **2222** and **80** port is not used
* download `./local/` directory from repo to your local `~/Projects/`:
```
mkdir ~/Projects  # if not exists
cd ~/Projects
git archive --remote=git@bitbucket.org:as-docker/projects-environment.git HEAD local/ | tar -x --strip=1
docker-compose build --pull --build-arg ID_RSA_PUB="$(cat ~/.ssh/id_rsa.pub)"
docker-compose pull
docker-compose up -d --force-recreate
```

* accept the certs for your SSH tunnel:
```
ssh -p 2222 root@localhost
The authenticity of host '[localhost]:2222 ([127.0.0.1]:2222)' can't be established.
ECDSA key fingerprint is SHA256:XXXXX.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '[localhost]:2222' (ECDSA) to the list of known hosts.
[...]
7a336c774889:~# exit
```

* configure Your local DNS (e.g. by dnsmasq) to handle `*.test` global domain to Your local IP `127.0.0.1`.
Example dnsmasq config for Ubuntu Linux 16.04
```
echo "address=/.test/127.0.0.1" | sudo tee /etc/NetworkManager/dnsmasq.d/010_test
sudo systemctl restart NetworkManager
```


Install (server) - deprecated! will be removed in future version
---
* try to keep the below structure on the home directory at server:
```
.
├── backup
├── data
└── www
    ├── example.com
    │   ├── docker
    │   ├── Yakefile
    │   └── docker-compose.yml
    ├── traefik
    │   ├── acme.json
    │   └── traefik.toml
    └── docker-compose.yml
```
First step is to setup `~/www` by:
```
git clone git@bitbucket.org:as-docker/projects-environment.git /tmp/env
mkdir ~/www
scp -r /tmp/env/prod/* ~/www
rm -rf /tmp/env
```
* run `docker network create global` once
* make sure server's **54**,  **80** and **443** port is not used
* run `docker-compose up -d`
* make sure the frontend containers have `traefik.enable: true` (due to `exposedByDefault = false` setting)
* configure dnsmasq to handle `CONTAINER_NAME` host (under the global domain `.docker`):
```
echo "server=/docker/127.0.0.1#54" >> /etc/dnsmasq.d/docker.conf
sudo service dnsmasq restart
```


HOWTO
---
* see `portainer` service (**label** section) as an example of `traefik` usage
* [local] use `ssh -p 2222 root@localhost` ssh tunel to connect to any container by `container_name` host.
* [server] run `dig -p 54 traefik.docker` to test DNS


Tools
---
* [YAKE](http://yake.amsdard.io/) command-line aliases management system

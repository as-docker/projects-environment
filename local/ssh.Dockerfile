FROM panubo/sshd:1.3.0

ARG ID_RSA_PUB

ENV SSH_ENABLE_ROOT=true
ENV TCP_FORWARDING=true

RUN echo "${ID_RSA_PUB}" >> /root/.ssh/authorized_keys
VOLUME /etc/ssh/keys
